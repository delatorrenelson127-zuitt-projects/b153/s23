db.courses.insertMany([
    {
        course: 'Basic HTML', 
        price: 1200.50, 
        isActive: true,
        description: 'Hypertext Markup Language',
        enrollees: ['12345', '123456','129285']
    },
    {
        course: 'Basic CSS', 
        price: 1500.50, 
        isActive: true,
        description: 'Cascading Sylesheet',
        enrollees: ['12345', '123456','129285']
    },
    {
        course: 'Basic Javascript', 
        price: 2000.50, 
        isActive: true,
        description: 'Cascading Sylesheet',
        enrollees: [ {
            userID : ObjectId("61fbc9af9d42a4a8386dd744")
        }]
    }
])