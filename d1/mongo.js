/* 
    Using MongoDB Locally:

    To Use MongoDB on your local machine, you must first open terminal and type the command 'mongod' to run the MongoDB Community server

    The server must always be running for the duration of the use of your local MongoDB interface/Shell

    Open a separate termial and type 'mongo' to open the Mongo Shell, which is a command line interface that allows us to issue MongoDB commands.


    MongoDB Commands:
    show dbs - show list of databases
    use <dbName> - create database

    // to finally create DB on MongoDB you need to insert atleast one record with this command.
    db.users.insertOne({
        name:'John Smith', 
        age: 20, 
        isActive: true
    })
    db.users.insertOne({
        name:'Jane Doe', 
        age: 21, 
        isActive: false
    })

    db.users.find() - to get a list of All 'users'


    // Within each database is a number of collections, where a multiple related data is tored (users, posts, products,ets)

    db.products.insertMany([
        {name: 'Product1', price: 200.50, stock:100, description: 'lorem ipsum'},
        {name: 'Product2', price: 333, stock: 25, description: 'lorem ipsum'}
    ])


    // To update one field
    db.users.updateOne(
        {_id : ObjectId("61fbcb049d42a4a8386dd745")},
        {$set:{isActive: true}}
    )

        db.users.updateOne(
        {_id : ObjectId("61fbcb049d42a4a8386dd745")},
        {$set:{address: 'U.S.A'}}
    )



    db.users.find().pretty()

    // Delete a document
    db.users.deleteOne({"_id" : ObjectId("61fbcb049d42a4a8386dd745") })
*/